### Dev App

* clonar o projeto:
```shell
git clone git@gitlab.com:rissi469/dev-app.git
```

* **Execução backend**:
```shell
cd backend
docker-compose -f "docker-compose.yml" up -d --build
```

* **Execução frontend**:
```shell
cd frontend
docker-compose -f "docker-compose.yml" up -d --build
```

Acessar http://localhost:3001 para executar a aplicação
