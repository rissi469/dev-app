import { AsyncContainerModule } from "inversify";
import { Repository } from "typeorm";
import { Developer } from "./entities/developer";
import { createConnection } from "typeorm";
import getRepository from "./repositories/getRepository";
import { TYPE } from "./constants/types";
import { DeveloperService } from "./services/developerService";

export const bindings = new AsyncContainerModule(async (bind) => {
  await createConnection();
  await require("./controllers/index");

  bind<Repository<Developer>>(TYPE.DeveloperRepository)
    .toDynamicValue(() => {
      return getRepository<Developer>(Developer);
    })
    .inRequestScope();

  bind<DeveloperService>(DeveloperService).toSelf();
});
