import moment from "moment";
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { Length } from "class-validator";
import { SexoEnum } from "../enums/sexoEnum";
import { BaseEntity } from "./baseEntity";

@Entity()
export class Developer extends BaseEntity {
  constructor(data: {
    id?: string;
    nome?: string;
    sexo?: SexoEnum;
    hobby?: string;
    dataNascimento?: Date;
  }) {
    super();
    if (!data) return;
    this.id = data.id;
    this.nome = data.nome;
    this.sexo = data.sexo;
    this.hobby = data.hobby;
    this.dataNascimento = data.dataNascimento;
  }

  @PrimaryGeneratedColumn("uuid")
  id?: string;

  @Column({
    type: "varchar",
  })
  @Length(3, 70)
  nome?: string;

  @Column({
    type: "enum",
    enum: SexoEnum,
  })
  sexo?: SexoEnum;

  @Column({ type: "int8" })
  idade?: number;

  @Column({
    type: "varchar",
  })
  @Length(3)
  hobby?: string;

  @Column({ type: "date" })
  dataNascimento?: Date;

  public calculateIdade() {
    this.idade = Math.floor(
      moment(new Date()).diff(moment(this.dataNascimento), "years", true)
    );
  }
}
