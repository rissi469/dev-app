import { body, param } from "express-validator";
import {
  interfaces,
  controller,
  httpGet,
  httpPost,
  requestBody,
  httpPut,
  requestParam,
  httpDelete,
} from "inversify-express-utils";
import { Controller } from "../@types/controller";
import { Developer } from "../entities/developer";
import { DeveloperService } from "../services/developerService";
import { SexoEnum } from "../enums/sexoEnum";

const postValidator = [
  body("nome")
    .notEmpty()
    .withMessage("Não foi informado o nome do desenvolvedor!"),
  body("hobby")
    .notEmpty()
    .withMessage("Não foi informado o nome do desenvolvedor!"),
  body("sexo")
    .custom((value, { req }) => {
      if (!value) {
        return false;
      }
      if (
        SexoEnum.FEMININO === req.body.sexo ||
        SexoEnum.MASCULINO === req.body.sexo
      ) {
        return true;
      }
      return false;
    })
    .withMessage("Sexo inválido"),
  body("dataNascimento")
    .isISO8601()
    .withMessage("A data de nascimento inserida não é válida!"),
];

const getById = [
  param("id").notEmpty().withMessage("Id informado não é válido!"),
];

@controller("/developers")
export class DeveloperController extends Controller {
  private readonly _developerService: DeveloperService;
  public constructor(developerService: DeveloperService) {
    super();
    this._developerService = developerService;
  }

  @httpGet("/")
  public async getAll(): Promise<interfaces.IHttpActionResult> {
    try {
      return this.ok(await this._developerService.findAll());
    } catch (e) {
      console.log(e);
      return this.internalServerError();
    }
  }

  @httpGet("/:id", ...getById)
  public async getById(
    @requestParam("id") id: string
  ): Promise<interfaces.IHttpActionResult> {
    try {
      const errosValidacao = this.validationError();

      if (errosValidacao) {
        return errosValidacao;
      }
      return this.ok(await this._developerService.findById(id));
    } catch (e) {
      console.log(e);
      return this.internalServerError();
    }
  }

  @httpPost("/", ...postValidator)
  public async save(
    @requestBody() developer: Developer
  ): Promise<interfaces.IHttpActionResult> {
    try {
      const errosValidacao = this.validationError();

      if (errosValidacao) {
        return errosValidacao;
      }
      return this.ok(await this._developerService.save(developer));
    } catch (e) {
      console.log(e);
      return this.internalServerError();
    }
  }

  @httpPut("/:id", ...[...postValidator, ...getById])
  public async update(
    @requestParam("id") id: string,
    @requestBody() developer: Developer
  ): Promise<interfaces.IHttpActionResult> {
    developer.id = id;
    try {
      const errosValidacao = this.validationError();

      if (errosValidacao) {
        return errosValidacao;
      }
      return this.ok(await this._developerService.save(developer));
    } catch (e) {
      console.log(e);
      return this.internalServerError();
    }
  }

  @httpDelete("/:id", ...getById)
  public async delete(
    @requestParam("id") id: string
  ): Promise<interfaces.IHttpActionResult> {
    try {
      const errosValidacao = this.validationError();

      if (errosValidacao) {
        return errosValidacao;
      }
      return this.ok(await this._developerService.deleteById(id));
    } catch (e) {
      console.log(e);
      return this.internalServerError();
    }
  }
}
