import { inject, injectable } from "inversify";
import { Repository } from "typeorm";
import { TYPE } from "../constants/types";
import { Developer } from "../entities/developer";

@injectable()
export class DeveloperService {
  private readonly _developerService: Repository<Developer>;
  public constructor(
    @inject(TYPE.DeveloperRepository) _developerService: Repository<Developer>
  ) {
    this._developerService = _developerService;
  }

  public save(developer: Developer): Promise<Developer> {
    const objDeveloper = new Developer(developer);
    objDeveloper.calculateIdade();
    return this._developerService.save(objDeveloper);
  }

  public findAll(): Promise<Developer[]> {
    return this._developerService.find();
  }

  public findById(id: string): Promise<Developer | undefined> {
    return this._developerService.findOne({
      where: {
        id,
      },
    });
  }

  public deleteById(id: string): void {
    this._developerService.delete(id);
  }
}
