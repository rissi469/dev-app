import React, { useEffect, useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Developer from "./pages/Developer";

function Routes() {
  const [base, setBase] = useState(process.env.REACT_APP_URL_BASE);

  useEffect(() => {
    if (process.env.REACT_APP_URL_BASE) {
      setBase(process.env.REACT_APP_URL_BASE);
    }
  }, []);

  return (
    <BrowserRouter basename={"/" + base}>
      <Switch>
        <Route path="/" exact component={Developer} />
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;
