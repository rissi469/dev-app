import { SexoEnum } from "../enums/sexoEnum";

export class Developer {
  id?: string;
  nome?: string;
  hobby?: string;
  sexo?: SexoEnum;
  dataNascimento?: Date;
}
