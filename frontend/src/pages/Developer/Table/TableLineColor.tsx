import React from "react";
import { withStyles } from "@material-ui/styles";
import { Table } from "@devexpress/dx-react-grid-material-ui";

const styles = (theme: any) => ({
  tableStriped: {
    "& tbody tr:nth-of-type(odd)": {
      backgroundColor: "rgba(204, 204, 204, 0.15)",
    },
  },
});

const TableComponentBase = ({ classes, ...restProps }: any) => (
  <Table.Table {...restProps} className={classes.tableStriped} />
);

export const TableLineColor = withStyles(styles, { name: "TableComponent" })(
  TableComponentBase
);
