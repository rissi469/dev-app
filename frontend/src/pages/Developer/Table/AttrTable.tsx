import { Table } from "@devexpress/dx-react-grid-material-ui";
const columns = [
  { name: "nome", title: "Nome" },
  { name: "hobby", title: "Hobby" },
  { name: "dataNascimento", title: "Nascimento" },
  { name: "sexo", title: "Sexo" },
  { name: "updated_at", title: "Ultima Alteração" },
  { name: "acoes", title: "Ações" },
];

const tableColumns: Table.ColumnExtension[] = [
  { columnName: "nome", align: "left" },
  { columnName: "hobby", align: "left" },
  { columnName: "dataNascimento", align: "left" },
  { columnName: "sexo", align: "left" },
  { columnName: "updated_at", align: "left" },
];

//sortingColumn
const sortingColumn = [{ columnName: "updatedDate", direction: "desc" }];

export { columns, tableColumns, sortingColumn };
