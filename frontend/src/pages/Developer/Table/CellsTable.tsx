import React from "react";
import { FaUserEdit, FaTrash } from "react-icons/fa";
import { Link } from "react-router-dom";
import { format } from "date-fns";
import parseISO from "date-fns/parseISO";
import { Table } from "@devexpress/dx-react-grid-material-ui";
import { SexoEnum } from "../../../enums/sexoEnum";

const CellEditar = (props: any) => {
  const { value, style, setDevEdit, deleteDev, row, ...restProps } = props;
  return (
    <Table.Cell
      {...restProps}
      className="cellsTableOther"
      style={{
        textAlign: "right",
        ...style,
      }}
    >
      <Link
        style={{ marginRight: 15 }}
        to="#"
        onClick={() => {
          setDevEdit(row);
        }}
      >
        <FaUserEdit size={22} color="#5668cc" />
      </Link>
      <Link
        style={{ marginRight: 15 }}
        to="#"
        onClick={() => {
          deleteDev(row);
        }}
      >
        <FaTrash size={22} color="#5668cc" />
      </Link>
    </Table.Cell>
  );
};

const CellSexo = (props: any) => {
  const { value, style, ...restProps } = props;
  return (
    <Table.Cell
      {...restProps}
      className="cellsTableOther"
      style={{
        ...style,
      }}
    >
      {value === SexoEnum.FEMININO ? "Feminino" : "Masculino"}
    </Table.Cell>
  );
};

const CellData = ({ value, style, row, ...restProps }: any) => {
  return (
    <Table.Cell
      {...restProps}
      className="cellsTableOther"
      style={{
        textAlign: "center",
        ...style,
      }}
    >
      {format(parseISO(value), "dd/MM/yyyy")}
    </Table.Cell>
  );
};

export const Cells = (props: any) => {
  if (props.column.name === "acoes") {
    return <CellEditar {...props} />;
  }

  if (
    props.column.name === "dataNascimento" ||
    props.column.name === "updated_at"
  ) {
    return <CellData {...props} />;
  }

  if (props.column.name === "sexo") {
    return <CellSexo {...props} />;
  }

  return <Table.Cell {...props} className="cellsTable" />;
};
