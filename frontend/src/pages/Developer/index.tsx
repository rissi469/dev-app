import DatePicker from "../../components/DatePicker";
import { Formik, FormikProps } from "formik";
import { MultiSelect } from "../../components/MultiSelect";
import {
  Grid as GridTable,
  Table,
} from "@devexpress/dx-react-grid-material-ui";
import { Grid, FormGroup, TextField, Typography } from "@material-ui/core";
import { useEffect, useRef, useState } from "react";
import { Developer } from "../../models/developer";
import { SexoEnum } from "../../enums/sexoEnum";
import Button from "../../components/Button";
import CardComponent from "../../components/CardComponent";
import BoxTitle from "../../components/BoxTitle";
import { Cells } from "./Table/CellsTable";
import { CircularProgress } from "@material-ui/core";
import { api } from "../../api/api";
import * as Yup from "yup";
import { columns, tableColumns } from "./Table/AttrTable";
import "./styles.css";
import { TableLineColor } from "./Table/TableLineColor";

function DeveloperPage() {
  const formRef = useRef<FormikProps<Developer>>(null);
  const [loadingSave, setLoadingSave] = useState<boolean>(false);
  const [devEdit, setDevEdit] = useState<Developer>({} as Developer);
  const [devs, setDevs] = useState<Developer[] | null>(null);
  const [dataNascimento, setDataNascimento] = useState<Date | null>(null);

  const getFromApi = async () => {
    setDevs(null);
    const response = await api.get("/developers/");
    if (response.status !== 200) {
      alert(response.data);
      return;
    }
    setDevs(response.data);
  };

  useEffect(() => {
    getFromApi();
  }, []);

  useEffect(() => {
    if (devEdit?.id) {
      formRef.current?.setFieldValue("id", devEdit.id);
      formRef.current?.setFieldValue("nome", devEdit.nome);
      formRef.current?.setFieldValue("hobby", devEdit.hobby);
      setDataNascimento(devEdit.dataNascimento || new Date());
      formRef.current?.setFieldValue(
        "sexo",
        optionsSexo.find((s) => {
          return s.value === devEdit.sexo;
        })
      );
    }
  }, [devEdit]);

  const validateDeveloper = () =>
    Yup.object().shape({
      nome: Yup.string()
        .required("Informe o nome")
        .min(3, "Mínimo de 3 caracteres"),
      hobby: Yup.string()
        .required("Informe o hobby")
        .min(3, "Mínimo de 3 caracteres"),
      sexo: Yup.mixed().required("Selecione o sexo"),
    });

  const clearForm = () => {
    formRef.current?.handleReset();
    setDataNascimento(null);
    setDevEdit({} as Developer);
  };

  const updateDev = async (developer: Developer) => {
    const sexo: any = developer.sexo;
    debugger;
    const response = await api.put(`/developers/${developer.id}`, {
      nome: developer.nome,
      hobby: developer.hobby,
      dataNascimento: dataNascimento,
      sexo: sexo?.value,
    });
    if (response.status === 200) {
      alert("Desenvolvedor Alterado com Sucesso!");
    } else {
      alert("Não foi possível alterar o dev");
    }
  };

  const saveDev = async (developer: Developer) => {
    const sexo: any = developer.sexo;
    const response = await api.post(`/developers/`, {
      nome: developer.nome,
      hobby: developer.hobby,
      dataNascimento: dataNascimento,
      sexo: sexo?.value,
    });
    if (response.status === 200) {
      alert("Desenvolvedor Salvo com Sucesso!");
    } else {
      alert("Não foi possível salvar o dev");
    }
  };

  const submitForm = async (developer: Developer) => {
    setLoadingSave(true);
    if (developer.id) {
      updateDev(developer);
    } else {
      saveDev(developer);
    }
    setLoadingSave(false);
    getFromApi();
    clearForm();
  };

  const deleteDev = async (developer: Developer) => {
    const response = await api.delete(`/developers/${developer.id}`);
    if (response.status === 200) {
      alert("Desenvolvedor apagado com sucesso!");
    } else {
      alert("Não foi possível apagadar o dev");
    }
    getFromApi();
  };

  const optionsSexo = [
    { label: "Feminino", value: SexoEnum.FEMININO },
    { label: "Masculino", value: SexoEnum.MASCULINO },
  ];
  return (
    <Grid container>
      <Grid item md={12} className="title">
        <Typography variant="h3" component="h4">
          Developers
        </Typography>
      </Grid>
      <Formik
        innerRef={formRef}
        enableReinitialize={true}
        initialValues={{
          id: undefined,
          nome: "",
          hobby: "",
          sexo: undefined,
          dataNascimento: undefined,
        }}
        onSubmit={(values: Developer) => {
          submitForm(values);
        }}
        validateOnChange={false}
        validateOnBlur={false}
        validationSchema={validateDeveloper()}
      >
        {({
          touched,
          errors,
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          isSubmitting,
          ...props
        }) => (
          <>
            <Grid container md={4}>
              <Grid item md={12} className="title">
                <Typography variant="h5" component="h6">
                  Formulário
                </Typography>
              </Grid>
              <Grid item md={6} xs={12}>
                <FormGroup>
                  <TextField
                    className="inputHeader"
                    label="Nome *"
                    placeholder="Digite o nome"
                    name="nome"
                    onChange={handleChange("nome")}
                    id="nome"
                    value={values.nome}
                    error={errors.nome ? true : false}
                    helperText={errors.nome}
                  />
                </FormGroup>
              </Grid>
              <Grid item md={6} xs={12}>
                <FormGroup>
                  <TextField
                    className="inputHeader"
                    label="Hobby *"
                    placeholder="Digite o hobby"
                    name="hobby"
                    onChange={handleChange("hobby")}
                    value={values.hobby}
                    id="hobby"
                    error={errors.hobby ? true : false}
                    helperText={errors.hobby}
                  />
                </FormGroup>
              </Grid>
              <Grid item md={6} xs={12}>
                <FormGroup>
                  <DatePicker
                    className="dateSelect"
                    value={dataNascimento}
                    label="Data Nascimento"
                    onChange={setDataNascimento}
                    error={false}
                    minDate={new Date()}
                    helperText={errors.dataNascimento}
                  />
                </FormGroup>
              </Grid>
              <Grid item md={6} xs={12}>
                <FormGroup>
                  <MultiSelect
                    className="multiSelectHeader"
                    id={"sexo"}
                    label={"Sexo"}
                    placeholder={"Selecione"}
                    optionLabel={"label"}
                    options={optionsSexo}
                    getOptionSelected={(option: any, value: any) =>
                      option.id === value.id
                    }
                    onChange={(e: any, value: any) => {
                      formRef.current?.setFieldValue("sexo", value);
                    }}
                    value={values.sexo}
                    error={touched.sexo && Boolean(errors.sexo)}
                    helperText={touched.sexo && errors.sexo}
                  />
                </FormGroup>
              </Grid>
              <Grid item md={12} className="buttons">
                <Button
                  color="default"
                  variant="contained"
                  type="reset"
                  onClick={clearForm}
                  buttonText="Cancelar"
                />
                <Button
                  color="primary"
                  variant="contained"
                  type="submit"
                  loading={loadingSave}
                  loadingText="Salvando"
                  onClick={() => {
                    handleSubmit();
                  }}
                  buttonText={devEdit.id ? "Atualizar" : "Salvar"}
                />
              </Grid>
            </Grid>
          </>
        )}
      </Formik>
      <Grid item md={8} xs={12}>
        <CardComponent>
          <BoxTitle title="Devs" />
          {devs !== null ? (
            <GridTable rows={devs} columns={columns}>
              <Table
                tableComponent={TableLineColor}
                cellComponent={(props) =>
                  Cells({ ...props, setDevEdit, deleteDev })
                }
                columnExtensions={tableColumns}
                rowComponent={({ row, ...restProps }: any) => {
                  return (
                    <Table.Row
                      {...restProps}
                      style={{
                        height: 25,
                      }}
                    />
                  );
                }}
                noDataCellComponent={(props: any) => {
                  return (
                    <td
                      colSpan={props.colSpan}
                      style={{ textAlign: "center", padding: 10 }}
                    >
                      Sem Registros
                    </td>
                  );
                }}
              />
            </GridTable>
          ) : (
            <Grid
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: 100,
              }}
            >
              <CircularProgress />
            </Grid>
          )}
        </CardComponent>
      </Grid>
    </Grid>
  );
}

export default DeveloperPage;
