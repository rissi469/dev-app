import React from "react";
import {
  Button as ButtonMaterial,
  CircularProgress,
  ButtonProps,
} from "@material-ui/core";
import { Link } from "react-router-dom";

interface ButtonRProps {
  buttonText: string | React.ReactNode;
  variant?: "contained" | "text" | "outlined" | undefined;
  link?: string;
  colorLink?: string;
  sizeCircularProgress?: number;
  margin?: number;
  loading?: boolean;
  loadingText?: string;
}

const Button: React.FC<ButtonRProps & ButtonProps> = ({
  buttonText,
  variant = "contained",
  link,
  colorLink = "white",
  sizeCircularProgress = 15,
  margin = 4,
  loading = false,
  loadingText,
  ...props
}) => {
  return (
    <ButtonMaterial
      variant={variant}
      style={{ margin }}
      disabled={loading}
      {...props}
    >
      {loading ? (
        <>
          <CircularProgress
            size={sizeCircularProgress}
            style={{ marginRight: 10 }}
          />
          {loadingText}
        </>
      ) : link ? (
        <Link style={{ textDecoration: "none", color: "#032C3C" }} to={link}>
          {buttonText}
        </Link>
      ) : (
        buttonText
      )}
    </ButtonMaterial>
  );
};

export default Button;
