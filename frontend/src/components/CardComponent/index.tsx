import React from "react";
import { Card } from "@material-ui/core";

import "./styles.css";

const CardComponent: React.FC = (props) => {
  return <Card className="cardComponents">{props.children}</Card>;
};

export default CardComponent;
