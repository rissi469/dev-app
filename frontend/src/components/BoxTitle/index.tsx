import React from "react";
import { Box, Typography } from "@material-ui/core";

import "./styles.css";

interface BoxTtitleProps {
  title: string;
}

const BoxTitle: React.FC<BoxTtitleProps> = (props) => {
  return (
    <div className="divBoxTitle">
      <Box className="boxTitleTable">
        <Typography variant="h4" className="TypographyH4" gutterBottom>
          {props.title}
        </Typography>
        {props.children}
      </Box>
    </div>
  );
};

export default BoxTitle;
