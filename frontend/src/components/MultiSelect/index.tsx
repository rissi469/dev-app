/* eslint-disable no-use-before-define */
import React from "react";
import Autocomplete, {
  AutocompleteChangeDetails,
  AutocompleteChangeReason,
  AutocompleteInputChangeReason,
} from "@material-ui/lab/Autocomplete";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

import "./styles.css";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // width: 250,
      "& > * + *": {
        marginTop: theme.spacing(3),
      },
    },
  })
);

interface MultiSelectProps {
  id?: string;
  className?: string;
  label: string;
  limitTags?: number | 2;
  placeholder: string;
  defaultValue?: any;
  options: any;
  optionLabel: string;
  inputValue?: string;
  onInputChange?: (
    event: React.ChangeEvent<{}>,
    value: string,
    reason: AutocompleteInputChangeReason
  ) => void;
  onChange: (
    event: React.ChangeEvent<{}>,
    value: any,
    reason: AutocompleteChangeReason,
    details?: AutocompleteChangeDetails<any> | undefined
  ) => void;
  value?: any;
  error?: boolean;
  helperText?: string | false;
  multiple?: boolean;
  disabled?: boolean;
  getOptionSelected?: ((option: any, value: any) => boolean) | undefined;
}

export const MultiSelect: React.FC<MultiSelectProps> = (
  props: MultiSelectProps
) => {
  const {
    id,
    className,
    label,
    limitTags,
    placeholder,
    defaultValue,
    options,
    optionLabel,
    onChange,
    value,
    inputValue,
    onInputChange,
    error,
    helperText,
    multiple,
    disabled,
    getOptionSelected,
  } = props;
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Autocomplete
        disabled={disabled}
        multiple={multiple}
        limitTags={limitTags}
        id={id}
        className={className}
        options={options}
        getOptionSelected={getOptionSelected}
        getOptionLabel={(option) => option[`${optionLabel}`]}
        defaultValue={defaultValue}
        value={multiple ? value ?? [] : value ?? null}
        onChange={onChange}
        inputValue={inputValue}
        onInputChange={onInputChange}
        renderInput={(params) => (
          <TextField
            {...params}
            label={label}
            placeholder={placeholder}
            error={error}
            helperText={helperText}
          />
        )}
      />
    </div>
  );
};
