import "date-fns";
import React from "react";
import DateFnsUtils from "@date-io/date-fns";
import ptBR from "date-fns/locale/pt-BR";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  KeyboardDatePickerProps,
} from "@material-ui/pickers";

const DatePicker: React.FC<KeyboardDatePickerProps> = (props) => {
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ptBR}>
      <KeyboardDatePicker
        {...props}
        disableToolbar
        variant="inline"
        invalidDateMessage="Formato de data inválido"
        format="dd/MM/yyyy"
        margin="normal"
        KeyboardButtonProps={{
          "aria-label": "change date",
        }}
      />
    </MuiPickersUtilsProvider>
  );
};

export default DatePicker;
